#!/bin/bash

######################################
#                                    #
# Gitscripts Common (Bash)           #
# Last Updated: 20.02.2018           #
# Author: Chanku/Sapeint             #
# Version: 1.0.0                     #
# License: MIT                       #
#                                    #
######################################
#                                    #
#  A collection of functions that    #
#  are used across all gitscripts    #
#                                    #
######################################

export LIB_GITSCRIPTS_COMMON_B_VERSION=(1 0 0)

########################################################
#                                                      #
# function: git_setup                                  #
# usage: git_setup                                     #
# example: git_setup                                   #
# args: None                                           #
#                                                      #
########################################################
#                                                      #
# Setups environment variables for the git scripts.    #
#                                                      #
######################################################## 
git_setup () {
    if [ -z "$GIT_USER" ]
    then
        export GIT_USER=git
    fi

    if [ -z "$GIT_ROOT" ]
    then
        export GIT_ROOT=/home/$GIT_USER
    fi;
}

########################################################
#                                                      #
# function: print_help                                 #
# usage: print_help name params ${switches[@])         #
# example: print_help "Help"" "[options]" "{arr[@]}"   #
# args: Script_Name (str) - Cannonical Name            #
#  Script_Usage_Params (str) - Params to show under    #
#                              usage                   #
#  Script_Switchces (arr str) - Array of strings to    #
#                           list options of a command  #
#                                                      #
#                                                      #
########################################################
#                                                      #
# Prints out a help menu/text                          #
#                                                      #
# WARNING: All of the switch elements must end in a    #
#          \n character to appear on the next line     #
#                                                      #
########################################################
print_help () {
    local script_name="$1"
    local script_usage_params="$2"
    local script_switches="$3"
    printf "$script_name\n"
    printf "Usage: $0 $script_usage_params\n"
    for i in "${script_switches[@]}"
    do
        printf " $i"
    done;
}

########################################################
#                                                      #
# function: check_exists_user                          #
# usage: check_exists_user usr inv msg exit            #
# example: check_exists_user "a" false "fail"          #
# args: user (str) - the user account to check         #
#       inv (bool) - check if the user doesn't exist   #
#       mesg (str) - Failure Message to print          #
#  exit_on_fail (opt) (bool) - exit script on failure  #
#                                                      #
########################################################
#                                                      #
# Checks if a given user account exists.               #
#                                                      #
########################################################
check_exists_user () {
    local username="$1"
    local inverse="$2"
    local failure_message="$3"
    local options=''
    if [ -z "$4" ]
    then
        local exit_on_failure=false
    else
        local exit_on_failure="$4"
    fi
    
    if ! $inverse
    then
        [ -d "$GIT_ROOT/$username" ]
        options=$?
    else
        [ ! -d "$GIT_ROOT/$username" ]
        options=$?
    fi

    if  [[ "$options" -eq 0 ]]
    then
        return 0
    else
        printf "$failure_message\n"
        if $exit_on_failure
        then
            exit 2
        fi
    fi;
}

########################################################
#                                                      #
# function: check_exists_repo                          #
# usage: check_exists_repo repo usr inv msg exit       #
# example: check_exists_repo "a" "a" false "fail"      #
# args: repo (str) - the Repository                    #
#       user (str) - the owner of the repository       #
#       inv (bool) - check if the repo doesn't exist   #
#       mesg (str) - Failure Message to print          #
#  exit_on_fail (opt) (bool) - exit script on failure  #
#                                                      #
########################################################
#                                                      #
# Checks if a repository exists.                       #
#                                                      #
########################################################
check_exists_repo () {
    local repo="$1"
    local username="$2"
    local inverse="$3"
    local failure_message="$4"
    local options=''
    if [ -z "$5" ]
    then
        local exit_on_failure=false
    else
        local exit_on_failure="$5"
    fi
    if ! $inverse
    then
        [ -d "$GIT_ROOT/$username/$repo" ]
        options=$?
    else
        [ ! -d "$GIT_ROOT/$username/$repo" ]
        options=$?
    fi

    if [ "$options" -eq 0 ]
    then
        return 0
    else
        printf "$failure_message\n"
        if $exit_on_failure
        then
            exit 3
        fi
    fi;
}
