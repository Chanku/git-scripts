# Git Scripts
  A small collection of scripts to manage a small git repo box

### Requirements
1. GNU Bash  
2. Linux  

### Installation
1. Do `git clone https://bitbucket.org/chanku/git-scripts.git`  
2. Setup a git user and modify what you need to in libgitscriptscommon.bash  
3. Register a user and then make a repository.  


## QnA  
### Why Bash?  
Because these started out rather quick and dirty, and I later rewrote them into what they are now. 

### Why GNU Bash?  
Because that's what my system has on it. However I've tried to be POSIX compilaint, to an extent, however I can't guarentee that I actually am. However I might get these to be POSIX compliant...one day.

### Should I use [INSERT GIT REMOTE SOFTWARE HERE]?  
Probably, these scripts are not the best, and mainly fit my usecase. You might need to modify them.

### What license can I use this under?  
See the LICENSE file

### Why write this?  
Because I have a VPS that I want to administrate in a certain way, and it has some git remotes on it. 
I wanted an easy, and somewhat automatic, way to manage the users and stuff, so I wrote these scripts.

### Can you support [INSERT ANOTHER VCS HERE]?  
No, I don't have the time or energy to do so. Feel free to take what I have here and fork it to make it work for whatever VCS. I'll even add a link here if you let me know about it!

### Can you create [INSERT IMPROVMENT  HERE]?  
Potentially, you can open an issue and suggest it, but the best way to make sure your feature is implemented is to submit a Pull/Merge Request

### How does this work?  
Basically, git supports ssh stuff already, and this merely leverages it. 

### Why not use an actual user-based approach?  
Because that would be a bit overkill for this, however if you wish to create a set of scripts feel free to do so, and I'll link them here!

### Why is the list at the end of the README?  
Because I wanted the stuff pertaining to what I wrote first

### Do I have to use Linux?  
No, but I can not guarentee it will work.

## Related Projects  
None so far.
